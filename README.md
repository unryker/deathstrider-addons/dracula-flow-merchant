### Notes
---
- Adds the Dracula Flow guy as a merchant skin for Deathstrider.
- Contains almost 200 lines.
- Dracula Flow cannot be trusted.
- This is very much a work in progress. To top this off, this is 200% a joke/shitpost mod. I don't really expect to finish this or put any serious effort in this. This is here just for the lolz.

### Credits
---
Graphical Assets
- Castlevania: Symphony of the Night
	- Dracula Flow's sprites.

Voice:
- John Davis Walker
	- Ripped straight out of Dracula Flow 1-4.

Special Thanks:
- PLUMMCORP RECORDS
	- Without them, Dracula Flow would've never existed.

### Known Issues
---
- Unfinished. See above in the Notes section. Artifacts are undecided, he has no custom hoverboard, and merchant behavior is subject to change.
- Some voice lines have laughter in them. I tried to clean them up to the best of my ability. Some other lines I had to omit because the quality was egregious.